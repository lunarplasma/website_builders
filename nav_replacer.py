from bs4 import BeautifulSoup

import os
import sys

# web_directory = "E:\\Projects\\web\\fixit_maths_web"
# template_file = "E:\\Projects\\web\\fixit_maths_web\\template.html"


def get_html_files(directory):
    for f in os.listdir(directory):
        if f.endswith(".html") and not f.startswith("_"): # and not f.startswith("template"):
            # print(os.path.join(directory, f))
            yield (os.path.join(directory, f))


def get_file_nav(file_path):
    html_doc = open(file_path)
    result = None
    try:
        soup = BeautifulSoup(html_doc, 'html5lib', from_encoding=html_doc.encoding)
        result = soup.nav
    except:
        print("Unexpected error:", sys.exc_info()[0], " file:", file_path)

    return result


def replace_nav_of_file(file_path, nav_tag):
    html_doc = open(file_path, "r+", encoding='cp1252')

    result = None
    try:
        soup = BeautifulSoup(html_doc, 'html5lib')
    except:
        print("Error opening: " + file_path)
        print("Unexpected error:", sys.exc_info()[0])

    if soup.nav is not None:
        soup.nav.replaceWith(nav_tag)
        print("Replacing nav in ", file_path)
        # now save back to file
        unsoup = str(soup) + '\n'
    else:
        raise Exception("Soup has no nav: " + file_path)

    try:
        html_doc.seek(0)
        html_doc.truncate()
        html_doc.write(unsoup)
    except:
        print("Unexpected error:", sys.exc_info()[0], " file:", file_path)
    finally:
        html_doc.close()

    return result


def replace_all_navs_in_dir(directory, template_file_path):
    # get htmls as a generator
    htmls = get_html_files(directory)

    try:
        new_nav = get_file_nav(template_file_path)
    except Exception as e:
        print(str(e))

    if new_nav is not None:
        # loop through files
        for file_path in htmls:
            try:
                replace_nav_of_file(file_path, new_nav)
            except:
                pass




def fixit_maths_replace_all_navs():
    pass


def test_htmls_in_folder(directory):
    htmls = get_html_files(directory)

    for file_path in htmls:
        html_doc = open(file_path, "r")
        try:
            soup = BeautifulSoup(html_doc, 'html5lib')
            print("File OK: " + file_path)
            html_doc.close()
        except:
            print("Error opening: " + file_path)
            print("Unexpected error:", sys.exc_info()[0])


def test_function():
    # test getting the tag
    template_file_path = "D:\\Projects\\Web\\fixit_maths_web\\template.html"
    new_nav = get_file_nav(template_file_path)

    test_file_path = "D:\\Projects\\Web\\fixit_maths_web\\test\\borrowing.html"
    replace_nav_of_file(test_file_path, new_nav)

    # print(get_file_nav(template_file_path))


# # test_function()
# replace_all_navs_in_dir(web_directory, template_file)