from node_builder import node_builder
from bs4 import BeautifulSoup as bs, Tag
from menu_formatter import create_menu_item
from nav_replacer import replace_all_navs_in_dir
from collections import OrderedDict
import argparse

html_list = []

def in_html_list(new_item):
    if new_item in html_list:
        print(" # Warning: " + new_item + " has been defined already")
    else:
        html_list.append(new_item)
        print(" - " + new_item)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create html menu.')
    parser.add_argument(
        '-f', dest="filename",
        default="menu.txt",
        help="The sourcefile for the menu")
    parser.add_argument('-o', dest="outfile", default="nav.html", help="The file to save as")
    parser.add_argument(
        '-r',
        dest="replacedir",
        default=None,
        help="If provided, replace all navs in all html files in target directory."
    )

    # get the nodes
    args = parser.parse_args()
    nodes = node_builder(args.filename)
    if nodes is None:
        raise RuntimeError("Invalid menu file: " + args.filename)

    # create a new soup
    soup = bs("", "html.parser")

    # create html from nodes
    nav = soup.new_tag('nav')
    nav['class'] = "navbar navbar-default borderless"
    navlist = soup.new_tag('ul')
    navlist['class'] = "nav navbar-nav"
    nav.append(navlist)
    soup.append(nav)

    print("Creating nav pages: ")

    for name0, node0 in nodes.items():
        # level 0
        if type(node0) is not OrderedDict:
            navlist.append(create_menu_item(soup, 0, name0, node0))
            in_html_list(node0)
        else:
            # level 1
            menu_item = create_menu_item(soup, 0, name0)
            submenu = menu_item.find('ul')
            navlist.append(menu_item)
            for name1, node1 in node0.items():
                if type(node1) is not OrderedDict:
                    submenu.append(create_menu_item(soup, 1, name1, node1))
                    in_html_list(node1)
                else:
                    submenu_item  = create_menu_item(soup, 1, name1, parent_text=name0)
                    subsubmenu = submenu_item.find('ul')
                    submenu.append(submenu_item)
                    for name2, node2 in node1.items():
                        subsubmenu.append(create_menu_item(soup, 2, name2, node2))
                        in_html_list(node2)


    # save the soup

    html = soup.prettify()
    # print (html)

    print("Writing to " + args.outfile)
    with open(args.outfile, mode="w",  encoding="utf-8") as file:
        file.write(html)
    print("Done")

    if args.replacedir is not None:
        print ("Replacing navs in: " + args.replacedir)
        try:
            replace_all_navs_in_dir(args.replacedir, args.outfile)
        except:
            pass

    print("\n\tMenudo complete.")

