import io
import re
from collections import OrderedDict

def __htmlify(key):
    pass1 = re.sub("[\\\/ ]", "-", key)
    pass2 = re.sub("[\(\)]", "_", pass1)
    pass3 = re.sub("[',]", "", pass2)
    pass4 = pass3.lower() + ".html"

    return pass4


def node_builder(target_file: str) -> OrderedDict:
    """
    Read a file and return a dictionary of nodes.
    Nodes are delineated by tabs.
    :param target_file: The text file to read.
    :return: Dictionary of nodes.
    """

    def recurser(file_handle: io.TextIOWrapper, current_level) -> dict:
        def get_nextline_level(file_handle: io.TextIOWrapper) -> int:
            file_pos = file_handle.tell()
            line = file_handle.readline()
            space_count = len(line) - len(line.lstrip(' '))
            line_level = int(space_count / 4)
            file_handle.seek(file_pos)
            return line_level

        sub_node = OrderedDict()

        line = "spare"
        line = file_handle.readline()
        while line is not '' and line is not '\n':

            nextline_level = get_nextline_level(file_handle)
            parsed_line = line.strip('\t').strip('\n').strip(' ')
            split_line = parsed_line.split(':')

            if nextline_level > current_level:
                # add a new subnode:
                sub_node[split_line[0]] = recurser(file_handle, nextline_level)
                nextline_level = get_nextline_level(file_handle)
            else:
                if len(split_line) > 1:
                    sub_node[split_line[0]] = split_line[1]
                else:
                    sub_node[split_line[0]] = __htmlify(split_line[0])

            if nextline_level < current_level:
                return sub_node

            line = file_handle.readline()

        return sub_node

    try:
        nodes = {}
        # read the file
        with open(target_file,  encoding='utf-8') as file_handle:
            nodes = recurser(file_handle, 0)
        return nodes

    except FileNotFoundError as e:
        print("Invalid file: " + target_file)
    except Exception as e:
        print("Exception:" + str(e))
