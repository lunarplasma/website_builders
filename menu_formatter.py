from bs4 import BeautifulSoup as bs, Tag
import re


def create_menu_item(soup: bs, level: int, text: str, href: str = None, parent_text=None):
    """
    Create a single menu item
    :param soup: The beautiful soup object
    :param level: The level of this item.
    :param text: The text to display for this item.
    :param href: The href for this item.
    :param parent_text: Text of the parent item
    :return: a new tag.
    """

    def ddl_title():
        """
        Clean up the text to be used as the data-dropdown toggle id.
        :return:
        """
        ddl_id = re.sub(r"[()//, ]", "", text).lower() + "DDL"
        if parent_text is not None:
            new_parent_text = re.sub(r"[()//, ]", "", parent_text).lower()
            ddl_id = new_parent_text + "-" + ddl_id
        return ddl_id

    def l0_link():
        """
        <li class="navbar-link">
            <a class="list-menu" href="index.html">Home</a>
        </li>
        """
        li = soup.new_tag('li')
        li['class'] = "navbar-link"
        a = soup.new_tag('a')
        a["class"] = "list-menu"
        a["href"] = href
        a.string = text
        li.append(a)
        return li

    def l0_dropdown():
        """
        <li class="nav-item dropdown hover-open">
            <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="areasOfMathsDDL">Areas of Maths
            <span class="caret"></span></a>
            <ul aria-labelledby="areasOfMathsDDL" class="nav-link dropdown-menu">
            </ul>
        </li>
        """
        li = soup.new_tag('li')
        li['class'] = "nav-item dropdown hover-open"

        a = soup.new_tag('a')
        a['class'] = "nav-link dropdown-toggle"
        a['aria-expanded'] = "false"
        a['aria-haspopup'] = "true"
        a['data-toggle'] = "dropdown"
        ddl = ddl_title()
        a['id'] = ddl
        a.string = text

        ul = soup.new_tag('ul')
        ul['class'] = "nav-link dropdown-menu"
        ul['aria-labelledby'] = ddl

        caret = soup.new_tag('span')
        caret['class'] = "caret"
        a.append(caret)

        li.append(a)
        li.append(ul)
        return li

    def l1_link():
        """
        <li class="dropdown-item">
            <a href="times-tables.html">Learning Times Tables</a>
        </li>
        """
        li = soup.new_tag('li')
        li['class'] = "dropdown-item"
        a = soup.new_tag('a')
        a["class"] = "list-menu"
        a["href"] = href
        a.string = text
        li.append(a)
        return li

    def l1_dropdown():
        """
        <li class="dropdown-item dropdown-submenu hover-open">
            <a aria-expanded="true" aria-haspopup="false" class="dropdown-toggle" data-toggle="dropdown" href="#" id="writtenMethodsDDL">Written Methods</a>
            <ul aria-labelledby="areasOfMathsDDL" class="dropdown-menu">
            </ul>
        </li>
        """
        li = soup.new_tag('li')
        li['class'] = "dropdown-item dropdown-submenu dropdown hover-open"

        a = soup.new_tag('a')
        a['class'] = "dropdown-toggle"
        a['aria-expanded'] = "false"
        a['aria-haspopup'] = "true"
        a['data-toggle'] = "dropdown"
        ddl = ddl_title()
        a['id'] = ddl
        a.string = text

        ul = soup.new_tag('ul')
        ul['class'] = "dropdown-menu"
        ul['aria-labelledby'] = ddl

        li.append(a)
        li.append(ul)
        return li

    def l2_link():
        """
        <li class="dropdown-item">
            <a href="subtraction-written-intro.html">Subtraction</a>
        </li>
        """
        li = soup.new_tag('li')
        li['class'] = "dropdown-item"
        a = soup.new_tag('a')
        a["class"] = "list-menu"
        a["href"] = href
        a.string = text
        li.append(a)
        return li

    new_tag = None

    if level is 0:
        if href is None:
            new_tag = l0_dropdown()
        else:
            new_tag = l0_link()
    elif level is 1:
        if href is None:
            new_tag = l1_dropdown()
        else:
            new_tag = l1_link()
    elif level is 2:
        if href is not None:
            new_tag = l2_link()
    else:
        raise Exception("Incorrect options: href=" + href + " level:" + str(level))

    return new_tag
