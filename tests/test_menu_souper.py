import unittest
import menudo


class TestMenuSouper(unittest.TestCase):
    def test_nodebuilder(self):
        # here is what test.txt contains:
        testDict = {
            "Main Item 1": {
                "SubItem1_1": {
                    "SubSubItem1_1_1": "page1.html",
                    "SubSubItem1_1_2": "page1.html"
                },
                "SubItem1_2": {
                    "SubSubItem1_2_1": "page2.html",
                    "SubSubItem1_2_2": "subsubitem1_2_2.html",
                    "SubSubItem1_2_3": "subsubitem1_2_3.html"
                },
                "SubItem1_3": "subitem1_3.html",
                "SubItem1_4": "subitem1_4.html"
            },
            "Main Item 2": {
                "SubItem2_1": "subitem2_1.html",
                "SubItem2_2": {
                    "SubSubItem2_2_1": "page3.html"
                },
                "Subitem2_3": {
                    "SubSubItem2_3_1": "subsubitem2_3_1.html",
                    "SubSubItem2_3_2": "subsubitem2_3_2.html",
                    "SubSubItem2_3_3": "page4.html",
                }
            }
        }

        testDict_small = {
            "Main Item 1" : {
                "SubItem1_1" : {
                    "SubSubItem1_1_1" : "subsubitem1_1_1.html",
                    "SubSubItem1_1_2" : "subsubitem1_1_2.html",
                    "SubSubItem1_1_3" : "subsubitem1_1_3.html"
                } ,
                "SubItem1_2" : "subitem1_2.html"
            },
            "Main Item 2" : "main-item-2.html"
        }

        testDict3 = {
            "menu 1":"menu-1.html",
            "1/2/3": "1-2-3.html",
            "menu(3)": {
                "submenu1":"submenu1.html",
                "submenu2": {
                    "subsubmenu1":"subsubmenu1.html",
                    "subsubmenu2":"subsubmenu2.html"
                },
                "sub(menu)3":"sub_menu_3.html"
            },
            "Menu 4":"menu-4.html"
        }

        self.maxDiff = None
        menu_nodes = menudo.node_builder("test2.txt")
        self.assertDictEqual(menu_nodes, testDict_small)

        menu_nodes = menudo.node_builder("test.txt")
        self.assertDictEqual(menu_nodes, testDict)

        menu_nodes = menudo.node_builder("test3.txt")
        self.assertDictEqual(menu_nodes, testDict3)

        menu_nodes = menudo.node_builder("..\menu.txt")
        pass